import { MlgApi } from 'mlg';
import image from './img.gif';

declare var Mlg: MlgApi;

const createElement = (right: boolean, flip: boolean) => {
    const imageElement = new Image(400, 400);
    imageElement.src = image;
    imageElement.style.position = 'fixed';
    imageElement.style.zIndex = '100000';
    imageElement.style.bottom = '0px';
    imageElement.style.pointerEvents = 'none';

    if (right) imageElement.style.right = '0px';
    else imageElement.style.left = '0px';

    if (flip) imageElement.style.transform = 'scaleX(-1)';

    return imageElement;
};

const left = createElement(false, false);
const right = createElement(true, true);

let active = false;
Mlg.registerEffect({
    name: 'rainbow-frogs-bottom-start',
    effect: (event) => {
        if (active) return;

        document.body.appendChild(left);
        document.body.appendChild(right);

        active = true;
    },
});

Mlg.registerEffect({
    name: 'rainbow-frogs-bottom-stop',
    effect: () => {
        if (!active) return;

        document.body.removeChild(left);
        document.body.removeChild(right);

        active = false;
    },
});
