var path = require('path');

module.exports = env => ({
  entry: './src/index.ts',
  mode: env.production ? 'production' : 'development',
  devtool: env.production ? 'source-map' : 'eval-source-map',
  output: {
    filename: 'mlg-basic-effects.js',
    clean: true
  },
  resolve: {
    extensions: ['.js', '.ts'],
    alias: {
      mlg: path.resolve(__dirname, '../mlg/src')
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        
        test: /\.(png|jpe?g|gif|mp3)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      }   
    ],
  }
});