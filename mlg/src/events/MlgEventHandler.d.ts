import { MlgEffect, MlgEffectName } from '../effects';
import { MlgEventName } from './types';
export default class MlgEventHandlers {
    private listeners;
    private observedElement;
    constructor(observedElement: EventTarget);
    bindEffect(eventName: MlgEventName, effectName: MlgEffectName, effect: MlgEffect, selector?: string): void;
    private createNewListener;
}
//# sourceMappingURL=MlgEventHandler.d.ts.map