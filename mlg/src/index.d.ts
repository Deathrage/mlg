import { MlgEffectManifest } from './effects';
import MlgInstance, { MlgConfig } from './MlgInstance';
declare global {
    interface Window {
        Mlg: MlgApi;
    }
}
export interface MlgApi {
    effects: MlgEffectManifest[];
    activeInstance: MlgInstance | undefined;
    init: (config: MlgConfig) => void;
    registerEffect: (manifest: MlgEffectManifest) => void;
}
//# sourceMappingURL=index.d.ts.map