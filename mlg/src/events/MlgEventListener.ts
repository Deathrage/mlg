import { MlgEffectName } from '../effects';

export type MlgEventListenerCallback = (event: Event) => void;

export default class MlgEventListener implements EventListenerObject {
    private callbacks: Record<MlgEffectName, MlgEventListenerCallback> = {};

    get effectNames() {
        return Object.keys(this.callbacks);
    }

    addCallback(effectName: MlgEffectName, callback: MlgEventListenerCallback) {
        this.callbacks[effectName] = callback;
    }

    handleEvent(event: Event): void {
        for (const callback of Object.values(this.callbacks)) {
            callback(event);
        }
    }
}
