import { MlgEffectManifest, MlgEffectsConfig } from './effects';
export interface MlgConfig {
    effects: MlgEffectsConfig;
}
export default class MlgInstance {
    private config;
    private eventHandlers;
    constructor(config: MlgConfig, observedElement: EventTarget);
    tryBindEffect({ effect, name: effectName }: MlgEffectManifest): boolean;
}
//# sourceMappingURL=MlgInstance.d.ts.map