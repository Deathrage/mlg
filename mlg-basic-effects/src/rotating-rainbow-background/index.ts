import { MlgApi } from 'mlg';
import image from './img.gif';

declare var Mlg: MlgApi;

let captured: {
    background: string;
    size: string;
} | null = null;

Mlg.registerEffect({
    name: 'rotating-rainbow-background-start',
    effect: () => {
        if (captured !== null) return;

        captured = {
            size: document.body.style.backgroundSize,
            background: document.body.style.background,
        };

        document.body.style.background = 'url(' + image + ') no-repeat';
        document.body.style.backgroundSize = 'cover';
    },
});

Mlg.registerEffect({
    name: 'rotating-rainbow-background-stop',
    effect: () => {
        if (captured === null) return;

        document.body.style.background = captured.background;
        document.body.style.backgroundSize = captured.size;

        captured = null;
    },
});
