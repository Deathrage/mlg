export type MlgCustomEventNames = 'effectadded';

export type MlgEventName = keyof DocumentEventMap | MlgCustomEventNames;
export type MlgElementSelector = string;
