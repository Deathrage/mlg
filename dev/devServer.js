const express = require('express');
const { spawn } = require('child_process');
const path = require('path');

const includedLibs = ['../mlg', '../mlg-basic-effects'];

console.log(process.cwd())
const watchers = includedLibs.map(lib => {
    const watcher = spawn('cd ' + lib + ' && npm run build:dev:watch', [], {
        shell: true
    });

    watcher.on('spawn', () => {
        console.log('Watching ' + lib);
    });
    watcher.on('error', data => {
        console.error(data.toString());
    });
    watcher.on('exit', () => console.log('Stopped watching ' + lib));

    watcher.stdout.on('data', data => console.log(data.toString()));
    watcher.stderr.on('data', data => console.error(data.toString()));
});

const app = express()
const port = process.env.PORT;
if (!port)
    throw new Error('Specify env variable PORT');

const testSite = process.env.TEST_SITE;
if (!testSite)
    throw new Error('Specify env variable TEST_SITE');

app.use(express.static(path.resolve(__dirname, testSite)));
for (const lib of includedLibs) {
    app.use(express.static(path.resolve(__dirname, lib, 'dist')));
}

app.use('/', (req, res) => res.sendFile(path.join(__dirname, testSite, 'index.html')));

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })