import { MlgElementSelector, MlgEventName } from './events/types';

export type MlgEffect = (event: Event) => void;
export type MlgEffectName = string;

export type MlgEffectConfig =
    | MlgEventName[]
    | Record<MlgEventName, MlgElementSelector>;

export interface MlgEffectManifest {
    name: MlgEffectName;
    effect: MlgEffect;
}

export type MlgEffectsConfig = Record<MlgEffectName, MlgEffectConfig>;
