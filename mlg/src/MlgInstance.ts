import { MlgEffectManifest, MlgEffectsConfig } from './effects';
import MlgInstanceEventHandlers from './events/MlgEventHandler';
import { MlgEventName } from './events/types';

export interface MlgConfig {
    effects: MlgEffectsConfig;
}

export default class MlgInstance {
    private config: MlgConfig;
    private eventHandlers: MlgInstanceEventHandlers;

    constructor(config: MlgConfig, observedElement: EventTarget) {
        this.config = config;
        this.eventHandlers = new MlgInstanceEventHandlers(observedElement);
    }

    tryBindEffect({ effect, name: effectName }: MlgEffectManifest) {
        const effectConfig = this.config.effects[effectName];

        // if given effect is not configured skip it
        if (!effectConfig) return false;

        // Effect configurations without selectors
        if (Array.isArray(effectConfig))
            for (const eventName of effectConfig)
                this.eventHandlers.bindEffect(eventName, effectName, effect);
        // Effect configuration with selectors
        else
            for (const [eventName, selector] of Object.entries(effectConfig))
                this.eventHandlers.bindEffect(
                    eventName as MlgEventName,
                    effectName,
                    effect,
                    selector
                );

        return true;
    }
}
