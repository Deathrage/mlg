import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import image from './img.png';
import sound from './sound.mp3';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound });

Mlg.registerEffect({
    name: 'hit-marker',
    effect: (event) => {
        if (!(event instanceof MouseEvent)) return;

        const { clientX: x, clientY: y } = event;

        const imageElement = new Image(50, 50);
        imageElement.src = image;
        imageElement.style.position = 'fixed';
        imageElement.style.zIndex = '100000';
        imageElement.style.top = y.toString() + 'px';
        imageElement.style.left = x.toString() + 'px';
        imageElement.style.transform = 'translateX(-50%) translateY(-50%)';
        imageElement.style.pointerEvents = 'none';

        howl.play();
        setTimeout(() => {
            document.body.appendChild(imageElement);

            setTimeout(() => {
                document.body.removeChild(imageElement);
            }, 100);
        }, 250);
    },
});
