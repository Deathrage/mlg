import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import sound from './sound.mp3';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound, volume: 0.5 });

let playing = false;

howl.on('end', () => (playing = false));

Mlg.registerEffect({
    name: 'baby-its-triple',
    effect: () => {
        if (playing) return;

        howl.play();
    },
});
