import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import sound from './sound.mp3';
import image from './img.gif';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound });

let gif: Blob;
fetch(image).then((res) =>
    res.blob().then((blob) => {
        gif = blob;
    })
);

Mlg.registerEffect({
    name: 'small-circle-explosion',
    effect: (event) => {
        if (!(event.target instanceof Element) || !gif) return;

        const { left, top, height, width } =
            event.target.getBoundingClientRect();
        const x = left + width / 2;
        const y = top + height / 2;

        console.log(event.target);

        const imageElement = new Image(200, 200);
        imageElement.src = window.URL.createObjectURL(gif);
        imageElement.style.position = 'fixed';
        imageElement.style.zIndex = '100000';
        imageElement.style.top = y.toString() + 'px';
        imageElement.style.left = x.toString() + 'px';
        imageElement.style.transform = 'translateX(-50%) translateY(-50%)';
        imageElement.style.pointerEvents = 'none';

        howl.stop();
        howl.play();

        document.body.appendChild(imageElement);
        setTimeout(() => {
            document.body.removeChild(imageElement);
            window.URL.revokeObjectURL(imageElement.src);
        }, 800);
    },
});
