import { MlgEffectName } from '../effects';
export declare type MlgEventListenerCallback = (event: Event) => void;
export default class MlgEventListener implements EventListenerObject {
    private callbacks;
    get effectNames(): string[];
    addCallback(effectName: MlgEffectName, callback: MlgEventListenerCallback): void;
    handleEvent(event: Event): void;
}
//# sourceMappingURL=MlgEventListener.d.ts.map