import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import sound from './sound.mp3';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound });

Mlg.registerEffect({
    name: 'elevator-music-start',
    effect: () => {
        howl.play();
    },
});

Mlg.registerEffect({
    name: 'elevator-music-stop',
    effect: () => {
        howl.stop();
    },
});
