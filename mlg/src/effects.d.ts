import { MlgElementSelector, MlgEventName } from './events/types';
export declare type MlgEffect = (event: Event) => void;
export declare type MlgEffectName = string;
export declare type MlgEffectConfig = MlgEventName[] | Record<MlgEventName, MlgElementSelector>;
export interface MlgEffectManifest {
    name: MlgEffectName;
    effect: MlgEffect;
}
export declare type MlgEffectsConfig = Record<MlgEffectName, MlgEffectConfig>;
//# sourceMappingURL=effects.d.ts.map