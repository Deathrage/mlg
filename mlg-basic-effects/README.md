# MLG Basic Effects

Pack of basic effects for [MLG](https://www.npmjs.com/package/mlg).

**Effect list:**
|Name|Description|Suggested Events|Audio|Visual
|----|----|----|---|---
| air-horn | sound of an air horn | all events |X||
| angular-rainbow-background-start | replaces body background for animated angular rainbow effect | all events ||X|
| angular-rainbow-background-stop | restores previous background if angular-rainbow-background is playing | all events ||X|
|baby-its-triple|yells "Baby it's triple! Oh yeah!" | all events|X||
|barely-alive-dead-link-start| starts playing Barely Alive - Dead Link | all events|X||
|barely-alive-dead-link-stop| stops playing Barely Alive - Dead Link if playing | all events|X||
|elevator-music-start|starts playing nice elevator music | all events|X||
|elevator-music-stop|stops playing nice elevator music if playing | all events|X||
|hit-marker|displays MLG hit marker around cursor| mouse events |X|X|
|rainbow-frogs-bottom-start|displays two animated MLG frogs on the bottom of the screen| all events||X|
|rainbow-frogs-bottom-stop|removes MLG frogs if present | all events ||X|
|rotating-rainbow-background-start|replaces body background for animated rotation rainbow effect | all events ||X|
|rotating-rainbow-background-stop|restore previous background if rotating-rainbow-background is playing | all events ||X|
|small-circle-explosion|creates small explosion around the mouse cursor|mouse events|X|X|
|sniper-quick-scope|performs quick scope with sniper rifle on the mouse cursor|mouse events|X|X|
