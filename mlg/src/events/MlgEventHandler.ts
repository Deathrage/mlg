import { MlgEffect, MlgEffectName } from '../effects';
import MlgEventListener, { MlgEventListenerCallback } from './MlgEventListener';
import { MlgEventName } from './types';

export default class MlgEventHandlers {
    private listeners = new Map<MlgEventName, MlgEventListener>();

    private observedElement: EventTarget;

    constructor(observedElement: EventTarget) {
        this.observedElement = observedElement;
    }

    bindEffect(
        eventName: MlgEventName,
        effectName: MlgEffectName,
        effect: MlgEffect,
        selector?: string
    ) {
        const callback: MlgEventListenerCallback = (event) => {
            // If DOMSelector is specified and the target element does not match the selector, skip it
            if (
                selector &&
                event.target &&
                event.target instanceof Element &&
                !event.target.matches(selector)
            )
                return;

            effect(event);
        };

        const listener =
            this.listeners.get(eventName) ?? this.createNewListener(eventName);

        listener.addCallback(effectName, callback);

        // Notify new effect addition to other effects

        this.observedElement.dispatchEvent(
            new CustomEvent<MlgEffectName>('effectadded', {
                detail: effectName,
            })
        );
    }

    private createNewListener(eventName: MlgEventName) {
        const listener = new MlgEventListener();

        this.observedElement.addEventListener(eventName, listener, {
            capture: true,
        });

        this.listeners.set(eventName, listener);

        return listener;
    }
}
