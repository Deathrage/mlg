import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import sound from './sound.mp3';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound });

let previous: number;

Mlg.registerEffect({
    name: 'air-horn',
    effect: () => {
        const previousCaptured = previous;
        if (previous) setTimeout(() => howl.stop(previousCaptured), 150);
        previous = howl.play();
    },
});
