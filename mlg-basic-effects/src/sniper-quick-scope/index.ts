import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import sound from './sound.mp3';
import image from './img.gif';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound });

let gif: Blob;
fetch(image).then((res) =>
    res.blob().then((blob) => {
        gif = blob;
    })
);

Mlg.registerEffect({
    name: 'sniper-quick-scope',
    effect: (event) => {
        if (!(event instanceof MouseEvent) || !gif) return;

        const x = event.clientX;
        const y = event.clientY;

        console.log(event.target);

        const imageElement = new Image();
        imageElement.src = window.URL.createObjectURL(gif);
        imageElement.style.position = 'fixed';
        imageElement.style.zIndex = '100000';
        imageElement.style.top = y.toString() + 'px';
        imageElement.style.left = x.toString() + 'px';
        imageElement.style.transform = 'translateX(-51%) translateY(-48%)';
        imageElement.style.pointerEvents = 'none';

        setTimeout(() => howl.play(), 500);

        document.body.appendChild(imageElement);
        setTimeout(() => {
            document.body.removeChild(imageElement);
            window.URL.revokeObjectURL(imageElement.src);
        }, 2200);
    },
});
