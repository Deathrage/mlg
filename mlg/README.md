# MLG

Bring genuine MLG experience to live websites. React to DOM events and play various MLG effects. Each experience is unique as it is tailor in accordance to users' actions.

[What is MLG?](https://mlg-parody.fandom.com/wiki/MLG_Montage_Parody)

## Adding MLG

Use CDN link:

```
TODO: Host it somewhere

// index.tml
<script src="link-to-cdn" />
<script>
    Mlg.init(mlgConfig);
</script>
```

Download it from NPM

```
npm i --save mlg

// index.js
import 'mlg';

Mlg.init(mlgConfig);
```

MLG is automatically added to the global scope and can be accessed as `Mlg`.

## Configuring MLG

MLG experience has to be configured via `MlgConfig`. The main task of `MlgConfig` is to bind effects to certain events and elements. By default no effects are dispatched. Currently MLG captures events solely on `body` element;

Simple config:

```
const mlgConfig = {
    effects: {
        // Hit Marker effect will be played anytime a click event occurs anywhere
        'hit-marker': ['click'],
        // Wow effect will be played anytime a keydown or keyup occurs anywhere
        wow: ['keydown', 'keyup],
        // Air Horn effect will me payed only if change occurs on input of type password
        // or if submit event occurs on any element
        'air-horn': {
            change: 'input[type="password"]',
            submit: '*'
        },
        // etc...
    }

};

Mlg.init(mlgConfig);
```

Larger config:

```
Mlg.init({
    effects: {
        'hit-marker': {
            click: ':not([type="submit"])',
        },
        'air-horn': ['input'],
        'small-circle-explosion': {
            click: '[type="submit"], [type="submit"] *',
        },
        'elevator-music-stop': ['submit'],
        'rainbow-frog-screen-start': ['submit'],
        'rainbow-frog-screen-stop': ['click'],
        'barely-alive-dead-link-start': ['submit'],
        'barely-alive-dead-link-stop': ['click'],
        'rotating-rainbow-background-start': ['submit'],
        'rotating-rainbow-background-stop': ['click'],
        'sniper-quick-scope': {
            click: 'img',
        },
        'baby-its-triple': {
            click: 'h1',
        },
    },
});

```

General binding pattern is `effect-name : event-name[] | biding-object` where `binding-object` may be defined as map of `event-name : DOMSelector`

## Getting MLG Effect Pack

By default MLG knows no effects. In order to play effects an effect pack has to be added. Currently there is only one effect pack [MLG Basic Effects](https://www.npmjs.com/package/mlg-basic-effects) which is under construction.

### Adding existing MLG Effect Pack

Use CDN link:

```
TODO: Host it somewhere

// index.tml
<script src="link-to-cdn-mlg" />
<script src="link-to-cdn-effect-pack" />
```

When adding MLG Effect Pack using `<script>` tag, it has to be added after the tag that adds MLG itself. There is no requirement on adding the tag before tag with `Mlg.init`. New effects can be added dynamically during MLG's lifetime.

Download it from NPM

```
npm i --save mlg-basic-effects

// index.js
import 'mlg';
import 'mlg-basic-effects';

Mlg.init(mlgConfig);
```

The same rule applies to downloading from npm as `mlg` has to be imported before any effects.

### Creating custom MLG Effects

You may want to create your own effects. Each effect is described by `MlgEffectManifest` which contains all important information about the effect. Effects should be agnostic to the event which invokes them. However, sometimes it cannot be done as for example effect that relies on cursor position can work only with mouse events.

Sample manifest:

```
{
    name: 'my-new-effect',
    effect: event => {
        /* do something */
    }
}
```

The `effect` function is the effect itself, where it's logic is hidden. It gets the captured event as an argument. The function is expected to return nothing (void) and can do almost anything. It's up to you to decide how you want to handle concurrency, manage your asset and delay the execution.

When your manifest is ready it has to be registered. As `Mlg` is exposed globally (and MLG itself should be included before any effects) you can just call `Mlg.registerEffect`.

Example no.1:

```
// Simple effect that will play a sound
const audio = new Audio(/*url to audio*/);

Mlg.registerEffect({
    name: 'explosion',
    effect: () => {
        audio.play();
    }
});
```

Example no.2:

```
// You can register the effects in promise chains
const myAudioPromise = fetch(/*url to audio*/)
    .then(res => res.blob())
    .then(blob => URL.createObjectUrl(blob))
    .then(url => new Audio(url)).
    .then(audio => {
        Mlg.registerEffect({
            name: 'explosion',
            effect: () => {
                audio.play();
            }
        });
    });

```

Example no.3:

```
// You may split your effect into two, one as a start and second as a stop.
const audio = new Audio(/*url to some music*/);

Mlg.registerEffect({
    name: 'song-start',
    effect: () => {
        audio.play();
    }
});

Mlg.registerEffect({
    name: 'song-strop',
    effect: () => {
        audio.stop();
    }
});

// These configuration will play the sound between onload event on body and first click
{
    effects: {
        'song-start': {
            'onload': 'body'
        },
        'song-stop': ['click']
    }
}
```

### Creating custom MLG Effect Packs

Feel free to create your own packages of effects and share them via CDN or npm.

## TODO:

-   Custom dispatchers
    -   Timer dispatchers - emit custom event after timeout
    -   Reactive dispatchers - emit custom event after another event
-   Unregister effects
-   MLG can be initialized around other containers than `body`
-   Concurrent instances of MLG
-   Add support events to `MlgEffectManifest` to mitigate risk of broken effects
-   Create effect categories - combat code repetition in effect manifests.
-   Inject custom target element to the effect
