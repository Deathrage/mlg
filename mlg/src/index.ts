import { MlgEffectManifest } from './effects';
import MlgInstance, { MlgConfig } from './MlgInstance';

declare global {
    interface Window {
        Mlg: MlgApi;
    }
}

export interface MlgApi {
    effects: MlgEffectManifest[];

    activeInstance: MlgInstance | undefined;

    init: (config: MlgConfig) => void;

    registerEffect: (manifest: MlgEffectManifest) => void;
}

window.Mlg = {
    effects: [],

    activeInstance: undefined,

    init: (config: MlgConfig) => {
        window.Mlg.activeInstance = new MlgInstance(
            config,
            window.document.body
        );

        for (const effect of window.Mlg.effects)
            window.Mlg.activeInstance?.tryBindEffect(effect);
    },

    registerEffect: (manifest: MlgEffectManifest) => {
        window.Mlg.effects.push(manifest);

        window.Mlg.activeInstance?.tryBindEffect(manifest);
    },
};
