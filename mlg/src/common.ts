export class MlgError extends Error {
    constructor(message: string) {
        super(message);

        this.name = 'MlgError';
    }
}

export type Wildcard = '*';
export const isWildcard = (input: unknown): input is Wildcard => input === '*';

// export const assertInitialized = (upToProperty?: keyof typeof window.mlgInstance) => {
//     if (window.mlg && (!upToProperty || window.mlg[upToProperty])) return true;
//     throw new MlgError('Mlg has not been initialized.');
// };
