export declare type MlgCustomEventNames = 'effectadded';
export declare type MlgEventName = keyof DocumentEventMap | MlgCustomEventNames;
export declare type MlgElementSelector = string;
//# sourceMappingURL=types.d.ts.map