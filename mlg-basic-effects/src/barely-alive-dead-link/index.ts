import { Howl } from 'howler';
import { MlgApi } from 'mlg';
import sound from './sound.mp3';

declare var Mlg: MlgApi;

const howl = new Howl({ src: sound, loop: true, volume: 0.5 });

Mlg.registerEffect({
    name: 'barely-alive-dead-link-start',
    effect: () => {
        howl.play();
    },
});

Mlg.registerEffect({
    name: 'barely-alive-dead-link-stop',
    effect: () => {
        howl.stop();
    },
});
